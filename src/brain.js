"use strict"

var NLP = require('natural');
var Sentiment = require('sentiment');


module.exports = Brain;

function Brain() {
    this.classifier = new NLP.LogisticRegressionClassifier();
    this.minConfidence = 0.6; //Threshold to validate a phrase
}

Brain.prototype.teach = function (label, phrases) {
    phrases.forEach(function (phrase) {
        console.log('Ingesting example for ' + label + ': ' + phrase);
        this.classifier.addDocument(phrase.toLowerCase(), label);
    }.bind(this));
    return this;
};

Brain.prototype.think = function () {
    this.classifier.train();
    // save the classifier for later use
    var aPath = './src/classifier.json';
    this.classifier.save(aPath, function (err, classifier) {
        // the classifier is saved to the classifier.json file!
        console.log('Writing: Creating a Classifier file in SRC.');
    });

    return this;
};


Brain.prototype.interpret = function (phrase) {
    var guesses = this.classifier.getClassifications(phrase.toLowerCase());
    var guess = guesses.reduce(toMaxValue);
    return {
        probabilities: guesses,
        guess: guess.value > this.minConfidence ? guess.label : null
    };
};

Brain.prototype.invoke = function (skillCode,session) {

    // check the sentiment 
    var senti = new Sentiment();
    let result = senti.analyze(session.message.text);
    console.log("Sentiments =>",result);
    var emoticon = "";
    if (result.score != 0) {
        if(result.score>0){
            emoticon = '\u{0001F60A}';
            console.log(result.positive);
        }
        else{
            emoticon = '\u{0002639}';
            console.log(result.negative);
        }
    }
    
    try {
        console.log('Running skill code for ' + skillCode + '...');
        var returnMessage = ExecSkills(skillCode) + emoticon;
        session.send(returnMessage);
    } catch (err) {
        throw new Error('The invoked skill doesn\'t exist!');
    }
    
    //skillCode(skill, session);
    return this;
};

function toMaxValue(x, y) {
    return x && x.value > y.value ? x : y;
}

function ExecSkills(skillCode){
    
    switch (skillCode) {
        case "Hello":
            return "Hi, how may I help you "
            break;
        case "Welcome":
            return "You are welcome "
        case "Thanks":
            return "Thank you "
        default:
            return "I have not been trained for this phrase "
            break;
    }
}