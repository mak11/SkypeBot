"use strict"

var restify = require('restify');
var builder = require('botbuilder');
var Brain = require('./src/brain');
var config = require('./config');
var sentiment = require('sentiment');


var Bottie = {
    Brain: new Brain(),
}

//Custom phrases of keywords 
var myPhrases = {
    Hello:['hi','hello','wassup','hey'],
    Welcome:['thanks', 'thank you','thx'],
    Thanks:['beautiful','awesome', 'wonderful', 'well done']
}

//Train the bot with the above keywords
Bottie.Teach = Bottie.Brain.teach.bind(Bottie.Brain);
eachKey(myPhrases, Bottie.Teach);
Bottie.Brain.think();
function eachKey(object, callback) {
    Object.keys(object).forEach(function (key) {
        callback(key, object[key]);
    });
}

//FOR STD INPUT AND OUTPUT
// var stdin = process.openStdin();

// stdin.addListener("data", function(d) {
//     var interpretation = Bottie.Brain.interpret(d.toString().trim());
//     var senti = new sentiment();
//     console.log('Sentiment =>', senti.analyze(d.toString().trim()));
//     console.log('NLP =>',interpretation);
//     //Bottie.Brain.invoke(interpretation.guess, null);
//   });




// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.PORT || config.HTTP_PORT_LISTENER, function () {
    console.log('%s listening to %s', server.name, server.url);
});

// Create chat bot
var connector = new builder.ChatConnector({
    appId: config.APP_ID,
    appPassword: config.APP_PASS
});

var bot = new builder.UniversalBot(connector);

server.post('/api/messages', connector.listen());

bot.on('contactRelationUpdate', function (message) {
    if (message.action === 'add') {
        var name = message.user ? message.user.name : null;
        var reply = new builder.Message()
            .address(message.address)
            .text("Hello %s... Thanks for adding me.", name || 'there');
        bot.send(reply);
    } else {
        // delete their data
    }
});

bot.dialog('/', function (session) {
    var interpretation = Bottie.Brain.interpret(session.message.text);
    console.log('Bottie heard: ' + session.message.text);
    console.log('Bottie interpretation: ', interpretation);
    Bottie.Brain.invoke(interpretation.guess, session);
});