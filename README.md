**Please go through the following link in order to get microsoft APP_ID & APP_PASSWORD  :** 

https://docs.microsoft.com/en-us/azure/bot-service/bot-service-quickstart?view=azure-bot-service-3.0

**After getting APP_ID & APP_PASSWORD add it to the config.js file**




# Natural-language processing
___
![image|690x354](http://discuss.focalworks.in/uploads/default/optimized/1X/773c9993a86a6ea019512cf8cce4f5d54e8a75de_1_690x354.jpg)

Natural-language processing (NLP) is an area of computer science and artificial intelligence concerned with the interactions between computers and human (natural) languages, in particular how to program computers to process and analyze large amounts of natural language data. 
(Click here to read more about it)

**Simple Explanation:**
**NLP** can be defined as a processing and understanding of raw human data for example let's consider a small baby of age 1 or 2 year in front of you and you got a bottle of milk and you say to that kid _“hey do you want some milk baby”_ then all he hears is _“blah blah blah blah blah **MILK** blah”_ and he properly responds because he’s got a “picture” of a milk bottle in his head connected to the word **“MILK”** so even when you change the statement to “baby I got a **milk bottle** with me would you like to have it” the baby will respond in the same manner because he heard the word **_“MILK”_**.

![image|590x350](http://discuss.focalworks.in/uploads/default/original/1X/6c87ac6c27b063f05e9da2351b4d7b41f00c3573.jpg)

**Now let's take a real coding example :** There are lots of library/package/module available for NLP for C#, nodejs, python, etc.
I have used **nodejs** as my preferred language for creating AI bots, so there is a package called “**_natural_**” you can install it by using “**_npm install natural_**” (Source)

All you have to do is just define some phrases to the nlp function and it will detect accordingly

**Example pseudo code :** 

`myPhrases =
{
    DrinkMilk: [“bottle”,”milk”,”want”,”drink”]
    DontDrinkMilk: [“bottle”,”milk”,”dont”,”not”, “no”, “do not”,”drink”]
}`

```//The above variable myPhrases has two set of keys each having predefined set of words to consider while processing the data```

`InitializeNLP(myPhrases) //Initializes the nlp with the above variable to process the data.`

`result = ProcessData(“Hey do you want some milk”)`
`Print(result)`

`result = ProcessData(“Drink the milk from this bottle”)`
`Print(result);`

    //Output #1:
    {  probabilities: [  {label :  ‘DrinkMilk’ ,   score: 0.9929102}  , { label: ‘DontDrinkMilk’, score: 0.00512200}], guess: ‘DrinkMilk’   }

    //Output #2:
    { probabilities: [  {label :  ‘DrinkMilk’ ,   score: 0.959212}  , { label: ‘DontDrinkMilk’, score: 0.00512200}], guess: ‘DrinkMilk’   }


    result = ProcessData(“hey don't drink that milk”)
    Print(result)

    result = ProcessData(“don't drink the milk from that bottle”)
    Print(result)


    //Output #1
    {  probabilities: [  {label :  ‘DrinkMilk’ ,   score: 0.715689}  , { label: ‘DontDrinkMilk’, score: 0.00512200}], guess: ‘DontDrinkMilk’   }

    //Output #2
    {  probabilities: [  {label :  ‘DrinkMilk’ ,   score: 0.6562100}  , { label: ‘DontDrinkMilk’, score: 0.00512200}], guess: ‘DontDrinkMilk’   }


You can now respond or process the data based on the guesses made by NLP output by using a **switch case** or **if clause** e.g : 

    switch(result.guess) 
    { 
    case “DrinkMilk” : 
    return “Ok I will drink that milk”;
    break;
    case “DontDrinkMilk”:
        return “Ok I will not drink that milk”
    break;
    }

![image|388x405](http://discuss.focalworks.in/uploads/default/original/1X/c1d1f4669fa61614d0dbe2c60c80d0c814fd29d2.jpg)


# Sentiment Analysis
___
Well sentiment analysis can be used for multiple purposes in this generation of Artificial Intelligence.
Like nlp there are few libraries available to analyze sentiment from a given sentence.
For nodejs you can install package called “**sentiment**” by using command “**_npm install sentiment_**” (Source)

![image|690x360](http://discuss.focalworks.in/uploads/default/optimized/1X/1f9faaf5c7af969eb7e61da5136e2a97aa3ea727_1_690x360.jpg)

**Let's start with an example :**
_**Pseudo code :**_

    result = sentiment(“Your are stupid”) //a negative statement
    	
    result = sentiment(“You are awesome”) //a positive statement
    	
    //Output #1
    {
      score: -2,  comparative: -0.6666666666666666,
      tokens: [ 'you', 'are', 'stupid' ],  words: [ 'stupid' ],
      positive: [],  negative: [ 'stupid' ]
    }

    //Output #2
    {
      score: 4,  comparative: 1.3333333333333333,
      tokens: [ 'you', 'are', 'awesome' ],  words: [ awesome],
      positive: [‘awesome’],  negative: [ ]
    }

As you can see it detects the negative or positive statement from the given input and outputs the score accordingly. The score determines the weight of the negative or positive statement.
For example, if u say “_you are a dumb machine_” it will output score of -3 the more abusive word used the more negative scores same goes for the positive scoring system.

![image|540x303](http://discuss.focalworks.in/uploads/default/original/1X/7ff3436742b114139c670bd0d27eb717c7212e5f.jpg)

**Sentiment** module can be used to process data like a comment from forum website or from any e-commerce website to analyze the overall **sentiment** of a topic or a product.
In **AI bots** it can be used to respond with emojis based on the user statement, like for negative statement the bot can respond with a sad emoji (😒) and in case of a positive statement the bot can respond with a smiling emoji (😊)

___
# Conclusion
![image|690x372](http://discuss.focalworks.in/uploads/default/optimized/1X/2b5e5132b0889ffd8c380ca5b461bc3a0fc9536c_1_690x372.jpg)



#### Working demo of SkypeBot : https://drive.google.com/file/d/10L-NOcURTube9Aj7_1-_bPDQ6xSUCaId/view